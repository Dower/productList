<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Products;
use \App\Categories;
use \App\Variations;
use \DB;

class ProductsController extends Controller
{
	public function filter()
	{
		$categories = Categories::all();
    	$variations = Variations::all();

    	$checked = [];

		$products = DB::table('products')
			->leftJoin('categories_products', 'products.id', '=', 'categories_products.products_id');

		if(request()->has('category')){
			$first = true;
			foreach(request('category') as $category){
				array_push($checked, $category);
				if($first){
					$products = $products->where('categories_id', $category);
					$first = false;
				}
				else{
					$products = $products->orWhere('categories_id', $category);
				}
			}
		}

		if (request()->has('sort')){
			$products = $products->orderBy('name', request('sort'));
		}else { $products = $products->orderBy('name'); }

		if (request()->has('search')){
			$products = $products->where('name', 'LIKE', '%'.request('search').'%');
		}


		$products = $products->groupBy('products_id')->paginate(9)
				->appends([
					'category' => request('category'),
					'sort' => request('sort'),
				]);


		// dd($products);

		return view('products.all', compact(['products', 'variations', 'categories', 'checked']));
	}

	public function show($id)
	{
		$product = Products::find($id);
		$categories = $product->categories;

		$variations = $product->variations;

		return view('products.show', compact(['product', 'categories', 'variations']));
	}
}
