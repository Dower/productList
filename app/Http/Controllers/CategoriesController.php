<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Categories;
use \App\Products;
use \App\Variations;
use \DB;

class CategoriesController extends Controller
{
	public function all()
	{
		$categories = Categories::all();

		return view('categories.all', compact(['categories']));
	}

	public function products()
	{
		return $this->belongsToMany(Products::class);
	}
}
