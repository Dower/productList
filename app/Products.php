<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    public function categories()
    {
    	return $this->belongsToMany(Categories::class);
    }

    public function variations()
    {
    	return $this->hasMany(Variations::class);
    }
}
