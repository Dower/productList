@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"><strong>Kategorijos</strong></div>
                <div class="card-body">

                    <div class="container">
                        <div class="row">
                            @foreach( $categories as $category )
                            <div class="col-4 card-margin">
                                <div class="card max-height card-hover">
                                    <div class="card-body text-center">
                                        <img class="card-img-top" src="{{ $category->img }}" alt="Card image cap">
                                        <h5 class="card-title">{{ $category->name }}</h5>
                                        <p>Products: {{ count($category->products) }}</p>
                                        <a href="/products/?category%5B%5D={{ $category->id }}"  class="stretched-link"></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
