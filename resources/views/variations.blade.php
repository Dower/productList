@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"><strong>{{ $variation->name }} {{ $product->name }}</strong></div>
                <div class="card-body">

                    <div class="container">
                        <div class="row">
                            <div class="col-4">
                                <img class="card-img-top" src="{{ $product->img }}" alt="Card image cap">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
